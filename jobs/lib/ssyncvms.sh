#!/bin/sh

. ${LAMINAR_HOME}'/cfg/jobs/lib/vm.sh'

# Function parameters:
# 1 = List of architectures to build a package.
#     value: list of string (separated by spaces)
hyperjobs_simple_syncvms()
{
    case $1 in
    '')
a=${VM_DEFARCH}
        ;;
    *)
a=$1
        ;;
    esac

    for i in $a
    do
        for c in sync update
        do
            doas librechroot -n $i $c
        done
    done
    unset a c i
}
