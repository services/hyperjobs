#!/bin/sh

. ${LAMINAR_HOME}'/cfg/jobs/lib/vm.sh'

# Function parameters:
# 1 = List of architectures to build a package.
#     value: list of string (separated by spaces)
# 2 = Build a package with network mode.
#     value: 1 (true) or <any> (false, as default)
hyperjobs_simple_mkpkg()
{
    case $1 in
    '')
a=${VM_DEFARCH}
        ;;
    *)
a=$1
        ;;
    esac

    case $2 in
    1)
n='-N'
        ;;
    *)
n=
        ;;
    esac

    for i in $a
    do
        doas libremakepkg -n $i $n
    done
    unset a i n
}
