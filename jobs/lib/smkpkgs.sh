#!/bin/sh

. ${LAMINAR_HOME}'/cfg/jobs/lib/pkg.sh'
. ${LAMINAR_HOME}'/cfg/jobs/lib/smkpkg.sh'

# Function parameters:
# @ = List of packages to build.
#     value: each string by parameters.
hyperjobs_simple_mkpkgs()
{
    case $@ in
    '')
        printf 'I have nothing to do! :(\n'
        return 0
        ;;
    esac

w=${PWD}
    cd ${PKG_PATH}
    for p in $@
    do
        cd $p
        hyperjobs_simple_mkpkg 'i686 x86_64' 1
        cd '../..'
    done
    cd $w

    unset p w
}
