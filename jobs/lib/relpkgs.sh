#!/bin/sh

. ${LAMINAR_HOME}'/cfg/jobs/lib/vm.sh'

hyperjobs_relpkgs()
{
    doas -u 'sner' librerelease
    for vma in ${VM_LSARCH}
    do
        doas librechroot -n ${vma} 'clean-repo'
    done
}
